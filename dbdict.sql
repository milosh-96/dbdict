-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 11, 2016 at 02:32 PM
-- Server version: 5.5.39
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbdict`
--

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
`id` int(11) NOT NULL,
  `group` varchar(50) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `group`) VALUES
(1, 'Abstraktna'),
(2, 'Životinje'),
(3, 'Mačke');

-- --------------------------------------------------------

--
-- Table structure for table `records`
--

CREATE TABLE IF NOT EXISTS `records` (
`id` int(11) NOT NULL,
  `record` varchar(50) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `records`
--

INSERT INTO `records` (`id`, `record`) VALUES
(1, 'Duh'),
(2, 'Domaća mačka'),
(3, 'Ris');

-- --------------------------------------------------------

--
-- Table structure for table `records_groups`
--

CREATE TABLE IF NOT EXISTS `records_groups` (
`id` int(11) NOT NULL,
  `record_id` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `records_groups`
--

INSERT INTO `records_groups` (`id`, `record_id`, `group_id`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 2, 3),
(4, 3, 3),
(5, 3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `sub_groups`
--

CREATE TABLE IF NOT EXISTS `sub_groups` (
`id` int(11) NOT NULL,
  `sub_group` int(11) DEFAULT NULL,
  `parent_group` int(11) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `sub_groups`
--

INSERT INTO `sub_groups` (`id`, `sub_group`, `parent_group`) VALUES
(1, 3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `types`
--

CREATE TABLE IF NOT EXISTS `types` (
`id` int(11) NOT NULL,
  `type_name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `types`
--

INSERT INTO `types` (`id`, `type_name`) VALUES
(1, 'Imenica');

-- --------------------------------------------------------

--
-- Table structure for table `types_groups`
--

CREATE TABLE IF NOT EXISTS `types_groups` (
`id` int(11) NOT NULL,
  `group_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `types_groups`
--

INSERT INTO `types_groups` (`id`, `group_id`, `type_id`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `records`
--
ALTER TABLE `records`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `records_groups`
--
ALTER TABLE `records_groups`
 ADD PRIMARY KEY (`id`), ADD KEY `record` (`record_id`), ADD KEY `group` (`group_id`);

--
-- Indexes for table `sub_groups`
--
ALTER TABLE `sub_groups`
 ADD PRIMARY KEY (`id`), ADD KEY `parentgroup` (`parent_group`), ADD KEY `subgroup` (`sub_group`);

--
-- Indexes for table `types`
--
ALTER TABLE `types`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `types_groups`
--
ALTER TABLE `types_groups`
 ADD PRIMARY KEY (`id`), ADD KEY `typesrelation` (`type_id`), ADD KEY `grousrelation` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `records`
--
ALTER TABLE `records`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `records_groups`
--
ALTER TABLE `records_groups`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `sub_groups`
--
ALTER TABLE `sub_groups`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `types`
--
ALTER TABLE `types`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `types_groups`
--
ALTER TABLE `types_groups`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `records_groups`
--
ALTER TABLE `records_groups`
ADD CONSTRAINT `group` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`),
ADD CONSTRAINT `record` FOREIGN KEY (`record_id`) REFERENCES `records` (`id`);

--
-- Constraints for table `sub_groups`
--
ALTER TABLE `sub_groups`
ADD CONSTRAINT `parentgroup` FOREIGN KEY (`parent_group`) REFERENCES `groups` (`id`),
ADD CONSTRAINT `subgroup` FOREIGN KEY (`sub_group`) REFERENCES `groups` (`id`);

--
-- Constraints for table `types_groups`
--
ALTER TABLE `types_groups`
ADD CONSTRAINT `grousrelation` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`),
ADD CONSTRAINT `typesrelation` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
