<?php
	class Group {
		private $db;
		public $name;
		public $typeId;
		public $id;
		public $records = array();
		
		public function __construct($id,$dbFetch) {
			$this->db = $dbFetch;
			$result = $this->db->query("SELECT * FROM groups WHERE id = '$id'")->fetch_array();
			$this->name = $result['group'];
			$this->id = $result['id'];
		}	
		
		public function recordCount() {
			$counterQuery = $this->db->query("SELECT * FROM records_groups WHERE group_id = '$this->id'");
			return $counterQuery->num_rows;
			
		}
		
		public function Type() {
			$typeQuery = $this->db->query("SELECT * FROM types_groups WHERE group_id = '$this->id'")->fetch_array();
			
			$this->typeId = $typeQuery['type_id'];
			
			$type = new Type($this->typeId,$this->db);
			
			return $type->name;
			
		}
		
		public function RecordsArray() {
			$query = $this->db->query("SELECT record_id FROM records_groups WHERE group_id = '$this->id'");
			$i=0;
			while($row = $query->fetch_array()) {
				$record = new Record($row[0],$this->db);
				$this->records[$i]['id'] = $record->id;
				$this->records[$i]['name'] = $record->name;
				$i++;
			}
			return $this->records;
		}
	
	
	
	
	}
?>