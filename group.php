<?php 
	require_once("app/acConfig.php");
	require_once("models/Group.php");
	require_once("models/Type.php");
	require_once("models/Record.php");
	
	$gId = $_GET['id'];
	
	$group = new Group($gId,$db);
	echo '<h2>' .  $group->name . ' (' . $group->Type() . ')</h2>';
	
	foreach($group->RecordsArray() as $record) {
		
		echo '<a href="record.php?id=' . $record['id'] . '">' . $record['name'] . '</a><p>';
	}
	
?>